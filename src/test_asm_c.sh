#!/bin/bash
for FILTER in Offset Sharpen Cuadrados
do
  for IMP in c
  do
    echo "Ancho,CiclosConsumidos" > $FILTER.$IMP.out
    for IMG in ./imgs/*.bmp
    do
      for (( i = 0; i < 500; i++ )); do
        WIDTH=$(identify -format '%w' $IMG)
        N_CICLOS=$(./build/tp2 -v $FILTER -i $IMP $IMG)
        echo -n "$WIDTH," >> $FILTER.$IMP.out
        echo $N_CICLOS >> $FILTER.$IMP.out
      done
    done
  done
done
