#!/bin/bash
FILTER=$1
for IMP in asm c
do
  echo "Ancho,CiclosConsumidos" > px_cte.$FILTER.$IMP.out
  for IMG in ./imgs/pix_cte/*.bmp
  do
    for (( i = 0; i < 100; i++ )); do
      WIDTH=$(identify -format '%w' $IMG)
      if [[ "$FILTER" == 'Manchas' ]]; then
        DIAMETER=$2
        N_CICLOS=$(./build/tp2 -v $FILTER -i $IMP $IMG $DIAMETER)
      else
        N_CICLOS=$(./build/tp2 -v $FILTER -i $IMP $IMG)
      fi
      echo -n "$WIDTH," >> px_cte.$FILTER.$IMP.out
      echo $N_CICLOS >> px_cte.$FILTER.$IMP.out
    done
  done
done
