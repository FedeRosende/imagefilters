#!/bin/bash
echo "Ancho,CiclosConsumidos" > v2.Offset.asm.out
for IMG in ./imgs/cuadradas/*.bmp
do
  for (( i = 0; i < 200; i++ )); do
    WIDTH=$(identify -format '%w' $IMG)
    N_CICLOS=$(./build/tp2 -v Offset -i asm $IMG)
    echo -n "$WIDTH," >> v2.Offset.asm.out
    echo "$N_CICLOS" >> v2.Offset.asm.out
  done
done
