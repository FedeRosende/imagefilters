#!/bin/bash
echo "Ancho,CiclosConsumidos" > mov2.Sharpen.asm.out
for IMG in ./imgs/cuadradas/*.bmp
do
  for (( i = 0; i < 500; i++ )); do
    WIDTH=$(identify -format '%w' $IMG)
    N_CICLOS=$(./build/tp2 -v Sharpen -i asm $IMG)
    echo -n "$WIDTH," >> mov2.Sharpen.asm.out
    echo "$N_CICLOS" >> mov2.Sharpen.asm.out
  done
done
