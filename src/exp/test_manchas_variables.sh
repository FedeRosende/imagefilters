#!/bin/bash
echo "Ancho,CiclosConsumidos,Tamaño" > size.Manchas.asm.out
for SIZE in {1..200}
do
  for IMG in ./imgs/cuadradas/*.bmp
  do
    for (( i = 0; i < 10; i++ )); do
      WIDTH=$(identify -format '%w' $IMG)
      N_CICLOS=$(./build/tp2 -v Manchas -i asm $IMG $SIZE)
      echo -n "$WIDTH," >> size.Manchas.asm.out
      echo -n "$N_CICLOS," >> size.Manchas.asm.out
      echo "$SIZE" >> size.Manchas.asm.out
    done
  done
done
