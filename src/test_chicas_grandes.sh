#!/bin/bash
FILTER=$1
for IMP in asm c
do
  echo "Ancho,CiclosConsumidos" > chicas.$FILTER.$IMP.out
  for IMG in ./imgs/cuadradas/*.bmp
  do
    for (( i = 0; i < 500; i++ )); do
      WIDTH=$(identify -format '%w' $IMG)
      if [[ "$FILTER" == 'Manchas' ]]; then
        DIAMETER=$2
        N_CICLOS=$(./build/tp2 -v $FILTER -i $IMP $IMG $DIAMETER)
      else
        N_CICLOS=$(./build/tp2 -v $FILTER -i $IMP $IMG)
      fi
      echo -n "$WIDTH," >> chicas.$FILTER.$IMP.out
      echo $N_CICLOS >> chicas.$FILTER.$IMP.out
    done
  done
done
