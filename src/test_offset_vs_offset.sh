#!/bin/bash
echo "Ancho,CiclosConsumidos" > add.Offset.asm.out
for IMG in ./imgs/cuadradas_grandes/*.bmp
do
  for (( i = 0; i < 500; i++ )); do
    WIDTH=$(identify -format '%w' $IMG)
    N_CICLOS=$(./build/tp2 -v Offset -i asm $IMG)
    echo -n "$WIDTH," >> add.Offset.asm.out
    echo "$N_CICLOS" >> add.Offset.asm.out
  done
done
