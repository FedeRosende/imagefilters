#!/bin/bash
for IMP in asm c
do
  echo "Ancho,CiclosConsumidos" > px_cte.Offset.$IMP.out
  for IMG in ./imgs/pixels_constantes/*.bmp
  do
    for (( i = 0; i < 500; i++ )); do
      WIDTH=$(identify -format '%w' $IMG)
      if [[ "$FILTER" == 'Manchas' ]]; then
        DIAMETER=$2
        N_CICLOS=$(./build/tp2 -v Offset -i $IMP $IMG $DIAMETER)
      else
        N_CICLOS=$(./build/tp2 -v Offset -i $IMP $IMG)
      fi
      echo -n "$WIDTH," >> px_cte.Offset.$IMP.out
      echo $N_CICLOS >> px_cte.Offset.$IMP.out
    done
  done
done
