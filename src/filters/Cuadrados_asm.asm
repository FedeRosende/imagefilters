section .data
Align 16

const_128: db 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80

edge: dd 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000


section .text
global Cuadrados_asm
Cuadrados_asm:
;COMPLETAR
	;OJO src(-1, y), src(x, -1) y dst(x, -1), dst (-1, y)
	;rdi uns char *src
	;rsi uns char *dst
	;edx int width
	;ecx int height
	;r8d int src_row_size
	;r9d int dst_row_size
	push rbp ;alineada
	mov rbp, rsp
	push r13 ;desalineada
	push r15 ;alineada
	push r14 ;desalineada
	push r12

	xor r13, r13
	xor r14, r14
	mov r8d, r8d
	mov r9d, r9d
	mov ecx, ecx
	pxor xmm0, xmm0
  movdqa xmm15, [const_128]

	sub ecx, 7
	add r13d, 4

.ciclo:
	cmp r13d, ecx ;veo si ya iteré por todas las filas
	jge .resto
	xor r15, r15 ;en r15 está la columna que estoy procesando

		.ciclo_columnas:
			cmp r15, r8 ;veo si ya iteré por todas las columnas
			jge .sigo
			mov r10d, r13d ;muevo la cantidad de filas que ya vi a r10d
			imul r10d, r8d ;escalo la cantidad de filas con la cantidad de pixels en cada una
			mov r14, r15 ;muevo la cantidad de columnas que vi a r14
			add r14d, r10d ;en r14d queda la cantidad de pixels totales que ya procesé | la posición del que voy a procesar ahora

			movdqu xmm1, [rdi+r14] ; xmm1 = [a3|r3|g3|b3|a2|r2|g2|b2|a1|r1|g1|b1|a0|r0|g0|b0]
			add r14, r8
		  movdqu xmm2, [rdi+r14] ; xmm2 = [a7|r7|g7|b7|a6|r6|g6|b6|a5|r5|g5|b5|a4|r4|g4|b4]
			add r14, r8
			movdqu xmm3, [rdi+r14] ; xmm3 = [a11|r11|g11|b11|a10|r10|g10|b10|a9|r9|g9|b9|a8|r8|g8|b8]
			add r14, r8
      movdqu xmm4, [rdi+r14] ; xmm4 = [a15|r15|g15|b15|a14|r14|g14|b14|a13|r13|g13|b13|a12|r12|g12|b12]
      add r14, r8
      movdqu xmm5, [rdi+r14] ; xmm5 = [a19|r19|g19|b19|a18|r18|g18|b18|a17|r17|g17|b17|a16|r16|g16|b16]
      add r14, r8
      movdqu xmm6, [rdi+r14] ; xmm6 = [a23|r23|g23|b23|a22|r22|g22|b22|a21|r21|g21|b21|a20|r20|g20|b20]
      add r14, r8
      movdqu xmm7, [rdi+r14] ; xmm7 = [a27|r27|g27|b27|a26|r26|g26|b26|a25|r25|g25|b25|a24|r24|g24|b24]
      sub r14, r8
      sub r14, r8
      sub r14, r8
      sub r14, r8
      sub r14, r8
      sub r14, r8

      psubb xmm1, xmm15
      psubb xmm2, xmm15
      psubb xmm3, xmm15
      psubb xmm4, xmm15
      psubb xmm5, xmm15
      psubb xmm6, xmm15
      psubb xmm7, xmm15

; PIXEL 1

      movdqu xmm9, xmm1
      pcmpgtb xmm9, xmm2
      pand xmm1, xmm9
      pandn xmm9, xmm2
      por xmm1, xmm9

      movdqu xmm9, xmm1
      pcmpgtb xmm9, xmm3
      pand xmm1, xmm9
      pandn xmm9, xmm3
      por xmm1, xmm9

      movdqu xmm9, xmm1
      pcmpgtb xmm9, xmm4
      pand xmm1, xmm9
      pandn xmm9, xmm4
      por xmm1, xmm9

      movdqu xmm10, xmm1
      psrldq xmm10, 8
      movdqu xmm9, xmm1
      pcmpgtb xmm9, xmm10
      pand xmm1, xmm9
      pandn xmm9, xmm10
      por xmm1, xmm9

      movdqu xmm10, xmm1
      psrldq xmm10, 4
      movdqu xmm9, xmm1
      pcmpgtb xmm9, xmm10
      pand xmm1, xmm9
      pandn xmm9, xmm10
      por xmm1, xmm9

      paddb xmm1, xmm15

      movd eax, xmm1

			mov [rsi+r14], eax
      add r14, r8

; PIXEL 2

      movdqu xmm9, xmm2
      pcmpgtb xmm9, xmm3
      pand xmm2, xmm9
      pandn xmm9, xmm3
      por xmm2, xmm9

      movdqu xmm9, xmm2
      pcmpgtb xmm9, xmm4
      pand xmm2, xmm9
      pandn xmm9, xmm4
      por xmm2, xmm9

      movdqu xmm9, xmm2
      pcmpgtb xmm9, xmm5
      pand xmm2, xmm9
      pandn xmm9, xmm5
      por xmm2, xmm9

      movdqu xmm10, xmm2
      psrldq xmm10, 8
      movdqu xmm9, xmm2
      pcmpgtb xmm9, xmm10
      pand xmm2, xmm9
      pandn xmm9, xmm10
      por xmm2, xmm9

      movdqu xmm10, xmm2
      psrldq xmm10, 4
      movdqu xmm9, xmm2
      pcmpgtb xmm9, xmm10
      pand xmm2, xmm9
      pandn xmm9, xmm10
      por xmm2, xmm9

      paddb xmm2, xmm15

      movd eax, xmm2

      mov [rsi+r14], eax
      add r14, r8

; PIXEL 3

      movdqu xmm9, xmm3
      pcmpgtb xmm9, xmm4
      pand xmm3, xmm9
      pandn xmm9, xmm4
      por xmm3, xmm9

      movdqu xmm9, xmm3
      pcmpgtb xmm9, xmm5
      pand xmm3, xmm9
      pandn xmm9, xmm5
      por xmm3, xmm9

      movdqu xmm9, xmm3
      pcmpgtb xmm9, xmm6
      pand xmm3, xmm9
      pandn xmm9, xmm6
      por xmm3, xmm9

      movdqu xmm10, xmm3
      psrldq xmm10, 8
      movdqu xmm9, xmm3
      pcmpgtb xmm9, xmm10
      pand xmm3, xmm9
      pandn xmm9, xmm10
      por xmm3, xmm9

      movdqu xmm10, xmm3
      psrldq xmm10, 4
      movdqu xmm9, xmm3
      pcmpgtb xmm9, xmm10
      pand xmm3, xmm9
      pandn xmm9, xmm10
      por xmm3, xmm9

      paddb xmm3, xmm15

      movd eax, xmm3

      mov [rsi+r14], eax
      add r14, r8

; PIXEL 4

      movdqu xmm9, xmm4
      pcmpgtb xmm9, xmm5
      pand xmm4, xmm9
      pandn xmm9, xmm5
      por xmm4, xmm9

      movdqu xmm9, xmm4
      pcmpgtb xmm9, xmm6
      pand xmm4, xmm9
      pandn xmm9, xmm6
      por xmm4, xmm9

      movdqu xmm9, xmm4
      pcmpgtb xmm9, xmm7
      pand xmm4, xmm9
      pandn xmm9, xmm7
      por xmm4, xmm9

      movdqu xmm10, xmm4
      psrldq xmm10, 8
      movdqu xmm9, xmm4
      pcmpgtb xmm9, xmm10
      pand xmm4, xmm9
      pandn xmm9, xmm10
      por xmm4, xmm9

      movdqu xmm10, xmm4
      psrldq xmm10, 4
      movdqu xmm9, xmm4
      pcmpgtb xmm9, xmm10
      pand xmm4, xmm9
      pandn xmm9, xmm10
      por xmm4, xmm9

      paddb xmm4, xmm15

      movd eax, xmm4

      mov [rsi+r14], eax

			add r15, 4
			jmp .ciclo_columnas

.sigo:
	add r13d, 4
	jmp .ciclo



.resto:
	sub r13d, 4
	add ecx, 3

.ciclo_resto:
	cmp r13d, ecx ;veo si ya iteré por todas las filas
	jge .bordes
	xor r15, r15 ;en r15 está la columna que estoy procesando

		.ciclo_columnas_resto:
			cmp r15, r8 ;veo si ya iteré por todas las columnas
			jge .sigo_resto
			mov r10d, r13d ;muevo la cantidad de filas que ya vi a r10d
			imul r10d, r8d ;escalo la cantidad de filas con la cantidad de pixels en cada una
			mov r14, r15 ;muevo la cantidad de columnas que vi a r14
			add r14d, r10d ;en r14d queda la cantidad de pixels totales que ya procesé | la posición del que voy a procesar ahora

			movdqu xmm1, [rdi+r14] ; xmm1 = [a3|r3|g3|b3|a2|r2|g2|b2|a1|r1|g1|b1|a0|r0|g0|b0]
			add r14, r8
			movdqu xmm2, [rdi+r14] ; xmm2 = [a7|r7|g7|b7|a6|r6|g6|b6|a5|r5|g5|b5|a4|r4|g4|b4]
			add r14, r8
			movdqu xmm3, [rdi+r14] ; xmm3 = [a11|r11|g11|b11|a10|r10|g10|b10|a9|r9|g9|b9|a8|r8|g8|b8]
			add r14, r8
			movdqu xmm4, [rdi+r14] ; xmm4 = [a15|r15|g15|b15|a14|r14|g14|b14|a13|r13|g13|b13|a12|r12|g12|b12]

			sub r14, r8
			sub r14, r8
			sub r14, r8


			psubb xmm1, xmm15
			psubb xmm2, xmm15
			psubb xmm3, xmm15
			psubb xmm4, xmm15

; PIXEL 1

			movdqu xmm9, xmm1
			pcmpgtb xmm9, xmm2
			pand xmm1, xmm9
			pandn xmm9, xmm2
			por xmm1, xmm9

			movdqu xmm9, xmm1
			pcmpgtb xmm9, xmm3
			pand xmm1, xmm9
			pandn xmm9, xmm3
			por xmm1, xmm9

			movdqu xmm9, xmm1
			pcmpgtb xmm9, xmm4
			pand xmm1, xmm9
			pandn xmm9, xmm4
			por xmm1, xmm9

			movdqu xmm10, xmm1
			psrldq xmm10, 8
			movdqu xmm9, xmm1
			pcmpgtb xmm9, xmm10
			pand xmm1, xmm9
			pandn xmm9, xmm10
			por xmm1, xmm9

			movdqu xmm10, xmm1
			psrldq xmm10, 4
			movdqu xmm9, xmm1
			pcmpgtb xmm9, xmm10
			pand xmm1, xmm9
			pandn xmm9, xmm10
			por xmm1, xmm9

			paddb xmm1, xmm15

			movd eax, xmm1

			mov [rsi+r14], eax

			add r15, 4
			jmp .ciclo_columnas_resto

.sigo_resto:
	inc r13d
	jmp .ciclo_resto



.bordes:
	xor r15, r15
	mov r12, r9
	add ecx, 3
	imul r12d, ecx
  movdqa xmm15, [edge]

.ciclo_bordes_fila:
	cmp r15, r9
	je .bordes_columna
	movdqu [rsi+r15], xmm15
  add r15, r9
  movdqu [rsi+r15], xmm15
  add r15, r9
  movdqu [rsi+r15], xmm15
  add r15, r9
  movdqu [rsi+r15], xmm15
  sub r15, r9
  sub r15, r9
  sub r15, r9

	movdqu [rsi+r12], xmm15
  sub r12, r9
  movdqu [rsi+r12], xmm15
  sub r12, r9
  movdqu [rsi+r12], xmm15
  sub r12, r9
  movdqu [rsi+r12], xmm15
  add r12, r9
  add r12, r9
  add r12, r9

	add r12, 16
	add r15, 16
	jmp .ciclo_bordes_fila

.bordes_columna:
	xor r12, r12
	xor r15, r15
	add r12, r9

.ciclo_bordes_columna:
	cmp r15d, ecx
	je .fin
	movdqu [rsi+r12], xmm15
	add r12, r9
	sub r12, 16
	movdqu [rsi+r12], xmm15
	add r12, 16
	inc r15
	jmp .ciclo_bordes_columna

.fin:

	pop r12
	pop r14
	pop r15
	pop r13
	pop rbp
	ret
