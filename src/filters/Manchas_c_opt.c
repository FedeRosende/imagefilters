#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>
#include "../tp2.h"
#include "../helper/utils.h"

void Manchas_c(
    uint8_t *src,
    uint8_t *dst,
    int width,
    int height,
    int src_row_size,
    int dst_row_size,
    int n)
{
    bgra_t (*src_matrix)[(src_row_size+3)/4] = (bgra_t (*)[(src_row_size+3)/4]) src;
    bgra_t (*dst_matrix)[(dst_row_size+3)/4] = (bgra_t (*)[(dst_row_size+3)/4]) dst;

    // Manchas
    float cosenos[n];
    float senos[n];
    for (int i = 0; i < n; ++i){
        float ii = ((float)i/n)*2*3.14159;
        cosenos[i] = cos(ii);
        senos[i] = sin(ii);
    }
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            
            float ii = senos[i%n];
            float jj = cosenos[j%n];
            int x = (int)50*ii*jj-25;
            
            dst_matrix[i][j].b = SAT(src_matrix[i][j].b + x);
            dst_matrix[i][j].g = SAT(src_matrix[i][j].g + x);
            dst_matrix[i][j].r = SAT(src_matrix[i][j].r + x);
            dst_matrix[i][j].a = 255;
        }
    }
}

