section .data
Align 16

transparencia: dd 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000
negro: dd 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000

section .text

global Offset_asm
Offset_asm:                                                     ; no hago ningún call por eso r11 conserva filas a procesar
;void Offset_c(
;    uint8_t *src,                      rdi
;    uint8_t *dst,                      rsi
;    int width,                         edx
;    int height,                        ecx
;    int src_row_size,                  r8d
;    int dst_row_size)                  r9d
    push rbp;
    mov rbp, rsp
    push r12
    push r13
    push r14
    push r15
    push rbx
    ;guardo los parámetros:
    mov r13, rdi 					; r13 = *src
    mov R14, rsi 					; r13 = *dst
    mov r15d, edx                   ; r15d = width
    mov ebx, ecx                    ; ebx = height
    ; limpio parte alta de parámetros
    mov r15d, r15d                  ; r15 = cols
    mov ebx, ebx                    ; rbx = filas
    ; la esquina superior izq es (0,0)
    ; empiezo desde la posición (9,9) de la imagen. Porque quiero dejar un marjen de 8 pixeles.
    shl r15, 5 						; r15 = r15 *32 quiero dejar 8 filas * 4 bytes por fila (margen superior)
    add r15, 32 					; r15 = r15 *32 + 32	(margen lateral)
    lea r13, [r13 + r15 ] 			; r13 = *src (9, 9) (dejo un márgen de 8 px)
    lea r14, [r14 + r15 ] 			; r14 = *dst (9, 9) (dejo un márgen de 8 px)
    sub r15, 32
    shr r15, 5

    ;calculo los pixeles a procesar por fila
    mov r12, r15
    sub r12, 16         ; r12 = pixeles a procesar por fila. Tiene un margen de 8 pixeles de cada lado.
    ;calculo las filas a procesar
    mov r11, rbx
    sub r11, 16         ; r11 = filas a procesar. Tiene un margen de 8 pixeles arriba y abajo
    ;recorro la imagen operando de a 4 pixeles.
    .loop:
        ;chequeo si me quedan filas por procesar
    	cmp r11, 0
    	je .finLoop
        ; me fijo si me quedan pixeles por procesar en ésta fila
        cmp r12, 0
        jne .procesarEnEStaFila
        ;No me quedan pixeles por procesar en esta fila, pero si más filas por procesar
    	jmp .quedanFilasPorProcesar
        .procesarEnEStaFila:
        ;proceso normalmente de a 4 pixeles (cada uno, en memoria: B, G, R, A entonces en los xmm: 128 A, R, G, B 0)
        ;en vez de modificar r13 y dps tener que volver a su estado inicial, voy a mover r13 a un registro r10, hacer las cuentas y así
        ;preservar el estado de r13.

        ;levanto cuatro pixeles hacia abajo desde la posición (i+8,j), shifteo para quedarme con los b. Uso un registro.
        shl r15, 5                                 ; r15*32 = #cols*32 (32 porque quiero dejar 8 filas * 4 px por fila)
        mov r10, r13
        add r10, r15                               ; r10 = r10 + r15*32 (*srx(i,j+8))
        movdqu xmm1, [r10]
        shr r15, 5

        ;shifteo para quedarme con los b

        pslld xmm1, 24                             ; shifteo 24 bits para quedarme con el b de cada pixel.
        psrld xmm1, 24                             ;xmm1 =|p(i+11,j)->B|p(i+10,j)->B|p(i+9,j)->B|p(i+8,j)->B|

        ;levanto cuatro pixeles hacia la derecha desde la posición (i,j+8), shifteo para quedarme con los g. Uso un registro.
        movdqu xmm2, [r13 + 32] 						; quiero levantar 4 píxeles a la derecha de donde estoy y cada posición de memoria almacena dos px

        ;shifteo para quedarme con los g
        ;fixme: experimentación: quizás shiftear en el sentido inverso hace que sea más rápido
        pslld xmm2, 16                             ; shifteo 16 bits para quedarme con el g de cada pixel
        psrld xmm2, 24
        pslld xmm2, 8                             ;xmm1 =|p(i+11,j)->G|p(i+10,j)->G|p(i+9,j)->G|p(i+8,j)->G|

       ;levanto cuatro pixeles hacia abajo a la derecha. desde la posición (i+8,j+8), shifteo para quedarme con los r. Uso un registro.
        shl r15, 5                                 ; r15*32 = #cols*32 (32 porque quiero dejar 8 filas * 4 px por fila)
        add r15, 32 							   ; para desplazarme 8 píxeles a la derecha
        mov r10, r13
        add r10, r15                               ; r10 = *src(i+8,j+8)
        movdqu xmm3, [r10]
        sub r15, 32
        shr r15, 5

        ;shifteo para quedarme con los r
        pslld xmm3, 8
        psrld xmm3, 24
        pslld xmm3, 16     ;xmm3 =|p(i+11,j)->R|p(i+10,j)->R|p(i+9,j)->R|p(i+8,j)->R|

        ;junto los tres registros en uno.
      	por xmm1, xmm2
        por xmm1, xmm3
       	;seteo la transparencia en 255
       	por xmm1, [transparencia]
        ;guardo en dst.
        movdqu [r14], xmm1
        ; avanzo los punteros de dst y src
        add r13, 16 					; procesé 4px y miden 4 bytes cada uno
        add r14, 16
        ; resto 4 a la cantidad de pixels por procesar
        sub r12, 4
		jmp .loop

        .quedanFilasPorProcesar:
        ;avanzo a la siguiente fila. Estoy en (i,m-8). El primer pixel del margen negro que tengo que dejar a la derecha.
        ;voy a pintar 16 pixeles de negro (el borde derecho de esta fila y el izquiero de la siguiente).
        ; 64 bytes
        mov r8, 64
  		movdqu xmm1, [negro]
        .loopLateralesNegros:
       		cmp r8, 0
       		je .finLoopLateralesNegros
        	movdqu [r14], xmm1
   	    	add r14, 4
        	sub r8, 4
        	jmp .loopLateralesNegros
   	    .finLoopLateralesNegros:
        ;calculo los pixeles a procesar por fila
        ; avanzo src a la siguiente fila. (i+1, 9)
        add r13, 64
		;calculo los pixeles a procesar por fila
    	mov r12, r15
    	sub r12, 16         ; r12 = pixeles a procesar por fila. Tiene un margen de 8 pixeles de cada lado.
    	;resto 1 a las filas por procesar
   		dec r11
   		;Proceso normalmente (vuelvo al loop).
        jmp .loop
    .finLoop:
    ;ya procesé toda la imagen
    ;pintar bordes superior e ingerior de 8 px de negro
    mov r8d, edx
    mov r8d, r8d 				; r8 = #cols
    shl r8, 5 					; r8 = #bytesAPintar =  #cols*8*4 quiero dejar 8 pixeles de márgen
    mov r9, rsi 			; r9 = *dst(0,0)
    mov r10d, edx
    mov r10d, r10d 			; r10 = #cols
    mov eax, ecx
    mov eax, eax 			; rax = #filas
    sub rax, 8 				; rax = #filas-8
    imul r10 				; rax = (#filas-8)*#cols
  	shl rax, 2 				; rax = (#filas-8)*#cols*4bytes
  	lea r10, [rsi + rax] 	; r10 = *dst(n-8,0)
  	movdqu xmm1, [negro]
        .loopArribaAbajoNegro:
       		cmp r8, 0
       		je .finLoopArribaAbajoNegro
        	;pinto borde superior:
        	movdqu [r9], xmm1
        	;pinto borde inferior:
        	movdqu [r10], xmm1
        	add r9, 16
        	add r10, 16
        	sub r8, 16
        	jmp .loopArribaAbajoNegro
    .finLoopArribaAbajoNegro:
    ;pinto los primeros 8 pixeles de la novena fila de negro. Es decir 32 bytes.
    movdqu[r9], xmm1
    add r9, 16
    movdqu[r9], xmm1
    add r9, 16

    pop rbx
    pop r15
    pop r14
    pop r13
    pop r12
    pop rbp;
ret
