section .data
Align 16

edge: dd 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000

section .text
global Sharpen_asm
Sharpen_asm:
;COMPLETAR
	;OJO src(-1, y), src(x, -1) y dst(x, -1), dst (-1, y)
	;rdi uns char *src
	;rsi uns char *dst
	;edx int width
	;ecx int height
	;r8d int src_row_size
	;r9d int dst_row_size
	push rbp ;alineada
	mov rbp, rsp
	push r13 ;desalineada
	push r15 ;alineada
	push r14 ;desalineada
	push r12

	xor r13, r13
	xor r14, r14
	mov r8d, r8d
	mov r9d, r9d
	mov ecx, ecx
	pxor xmm0, xmm0

	movdqa xmm15, [edge]

	sub ecx, 2
	mov rdx, r8
	sub rdx, 8

.ciclo:
	cmp r13, rcx ;veo si ya iteré por todas las filas
	je .bordes
	xor r15, r15 ;en r15 está la columna que estoy procesando

		.ciclo_columnas:
			cmp r15, rdx ;veo si ya iteré por todas las columnas
			jge .sigo
			mov r10d, r13d ;muevo la cantidad de filas que ya vi a r10d
			imul r10d, r8d ;escalo la cantidad de filas con la cantidad de pixels en cada una
			mov r14, r15 ;muevo la cantidad de columnas que vi a r14
			add r14d, r10d ;en r14d queda la cantidad de pixels totales que ya procesé | la posición del que voy a procesar ahora

			movdqu xmm1, [rdi+r14] ;traigo los primeros tres pixels | xmm1 = [a3|r3|g3|b3|a2|r2|g2|b2|a1|r1|g1|b1|a0|r0|g0|b0]
			add r14, r8
		  movdqu xmm2, [rdi+r14] ;traigo los segundos tres pixels | xmm2 = [a7|r7|g7|b7|a6|r6|g6|b6|a5|r5|g5|b5|a4|r4|g4|b4]
			add r14, r8
			movdqu xmm3, [rdi+r14] ;traigo los últimos tres pixels | xmm3 = [a11|r11|g11|b11|a10|r10|g10|b10|a9|r9|g9|b9|a8|r8|g8|b8]
			sub r14, r8
			sub r14, r8

			movdqu xmm4, xmm1
			movdqu xmm10, xmm1

			movdqu xmm5, xmm3
			movdqu xmm12, xmm3

			movdqu xmm9, xmm2
			movdqu xmm11, xmm2

;pixel 2

			punpckhbw xmm1, xmm0 ; xmm1 = [a3|r3|g3|b3|a2|r2|g2|b2]
			movdqu xmm6, xmm1 ;xmm6 = xmm1
			pslldq xmm6, 8 ; xmm6 = [a2|r2|g2|b2|0|0|0|0]
			punpcklbw xmm4, xmm0 ; xmm4 = [a1|r1|g1|b1|*|*|*|*]

			paddw xmm1, xmm4
		  paddw xmm1, xmm6 ; xmm1 = [a1+a2+a3|r1+r2+r3|g1+g2+g3|b1+b2+b3|*|*|*|*]

			punpckhbw xmm3, xmm0 ; xmm3 = [a11|r11|g11|b11|a10|r10|g10|b10]
			movdqu xmm7, xmm3 ; xmm7 = xmm3
			pslldq xmm7, 8 ; xmm7 = [a10|r10|g10|b10|0|0|0|0]
			punpcklbw xmm5, xmm0 ; xmm5 = [a9|r9|g9|b9|*|*|*|*]

			paddw xmm3, xmm5
			paddw xmm3, xmm7 ; xmm3 = [a9+a10+a11|r9+r10+r11|g9+g10+g11|b9+b10+b11|*|*|*|*]

			paddw xmm1, xmm3

			punpckhbw xmm2, xmm0 ; xmm2 = [a7|r7|g7|b7|a6|r6|g6|b6]
		  movdqu xmm8, xmm2 ; xmm8 = xmm2
			pslldq xmm8, 8 ; xmm8 = [a6|r6|g6|b6|0|0|0|0]
			punpcklbw xmm9, xmm0 ; xmm9 = [a5|r5|g5|b5|*|*|*|*]

			paddw xmm2, xmm9 ; xmm2 = [a7+a5|r7+r7|g7+g7|b7+b7|*|*|*|*]
			paddw xmm1, xmm2 ; sumo todos los valores en xmm1 menos los los del pixel que calculo

			movdqu xmm0, xmm8 ; copio xmm8
			psllw xmm8, 3 ; multiplico por 8
			paddusw xmm8, xmm0 ; lo sumo una vez más para multiplicar por 9 | xmm8 = [9a6|9r6|9g6|9b6|*|*|*|*]

			psubusw xmm8, xmm1 ; en la parte alta de xmm8 me queda el valor del nuevo pixel

			psrldq xmm8, 8

			pxor xmm0, xmm0

			packuswb xmm8, xmm0

;pixel 1

			movdqu xmm1, xmm10
			movdqu xmm2, xmm11
			movdqu xmm3, xmm12

			; xmm1 = [a3|r3|g3|b3|a2|r2|g2|b2|a1|r1|g1|b1|a0|r0|g0|b0]

			; xmm2 = [a7|r7|g7|b7|a6|r6|g6|b6|a5|r5|g5|b5|a4|r4|g4|b4]

			; xmm3 = [a11|r11|g11|b11|a10|r10|g10|b10|a9|r9|g9|b9|a8|r8|g8|b8]

			punpcklbw xmm1, xmm0 ; xmm1 = [a1|r1|g1|b1|a0|r0|g0|b0]
			movdqu xmm13, xmm1
			pslldq xmm13, 8 ; xmm13 = [a0|r0|g0|b0|0|0|0|0]
			punpckhbw xmm10, xmm0
			pslldq xmm10, 8 ; xmm10 = [a2|r2|g2|b2|0|0|0|0]

			paddw xmm1, xmm13
			paddw xmm1, xmm10

			punpcklbw xmm3, xmm0 ; xmm3 = [a9|r9|g9|b9|a8|r8|g8|b8]
			movdqu xmm7, xmm3
			pslldq xmm7, 8 ; xmm15 = [a8|r8|g8|b8|0|0|0|0]
			punpckhbw xmm12, xmm0
			pslldq xmm12, 8 ; xmm12 = [a10|r10|g10|b10|0|0|0|0]

			paddw xmm3, xmm7
			paddw xmm3, xmm12

			paddw xmm1, xmm3

			punpcklbw xmm2, xmm0 ; xmm2 = [a5|r5|g5|b5|a4|r4|g4|b4] | esto lo multiplico por 9
			movdqu xmm14, xmm2
			pslldq xmm14, 8 ; xmm14 = [a4|r4|g4|b4|0|0|0|0]
			punpckhbw xmm11, xmm0
			pslldq xmm11, 8 ; xmm11 = [a6|r6|g6|b6|0|0|0|0]

			paddw xmm14, xmm11

			paddw xmm1, xmm14 ; en xmm1 queda la suma de todos los pixels menos el que calculo

			movdqu xmm3, xmm2
			psllw xmm2, 3 ; multiplico por 8
			paddusw xmm2, xmm3 ; sumo uno más y multiplico por 9

			psubusw xmm2, xmm1

			psrldq xmm2, 8

			packuswb xmm2, xmm0

 			movd eax, xmm2

			shl rax, 32
			shr rax, 32

			movd r12d, xmm8

			shl r12, 32

			or rax, r12

			add r14, r9
			add r14, 4

			mov [rsi+r14], rax

			add r15, 8
			jmp .ciclo_columnas

.sigo:
	add r13, 1
	jmp .ciclo

.bordes:
	xor r15, r15
	mov r12, r9
	add ecx, 1
	imul r12d, ecx

.ciclo_bordes_fila:
	cmp r15, r9
	je .bordes_columna
	movdqu [rsi+r15], xmm15
	movdqu [rsi+r12], xmm15
	add r12, 16
	add r15, 16
	jmp .ciclo_bordes_fila

.bordes_columna:
	mov eax, 0xFF000000
	xor r12, r12
	xor r15, r15
	add r12, r9

.ciclo_bordes_columna:
	cmp r15d, ecx
	je .fin
	mov [rsi+r12], eax
	add r12, r9
	sub r12, 4
	mov [rsi+r12], eax
	add r12, 4
	inc r15
	jmp .ciclo_bordes_columna

.fin:

	pop r12
	pop r14
	pop r15
	pop r13
	pop rbp
	ret
