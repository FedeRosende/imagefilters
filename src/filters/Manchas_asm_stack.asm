extern malloc
extern free

section .data
Align 16

n: dd 0.0
cincuenta: dd 50.0, 50.0, 50.0, 50.0
veinticinco: dd 25.0, 25.0, 25.0, 25.0
dos_pi: dd 6.28318530
alpha: dd 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000

section .text
global Manchas_asm
;void Manchas_c(
;	uint8_t *src,						rdi
;	uint8_t *dst,						rsi
;   int width,							edx
;   int height,							ecx
;   int src_row_size,					r8d (bytes)
;   int dst_row_size,					r9d (bytes)
;	n									pila
Manchas_asm:
  push rbp ;a
  mov rbp, rsp

  push r12 ;d
  push r13 ;a
  push r14 ;d
  push r15 ;a
  push rbx ;d

  mov r15, [rbp+16] ; r15d = n
  mov r15d, r15d ; limpio parte alta de r15
  mov r14, rdi ;r14 = src
  mov r13, rsi ;r13 = dst
  mov r12d, edx ;r12d = width
  mov ebx, ecx ;ebx = height

  ; Obtengo el primer múltiplo de 4 mayor a n en r11d
  xor r11, r11
  xor edx, edx
  mov eax, r15d
  mov r11d, 4
  div r11d ;edx = n (4)
  sub r11d, edx
  add r11d, r15d

  xor r8, r8
  mov r8d, r11d
  shl r8d, 3 ;r8d = primer multiplo * 2 vectores * 4 bytes
  push r8; a
  sub rsp, r8

  pxor xmm1, xmm1
  pxor xmm2, xmm2

  cvtsi2ss xmm1, r15d ;xmm1 = [0|0|0|n (como float)]
  mov ecx, [dos_pi]
  movd xmm2, ecx ;xmm2 = [0|0|0|2+pi]

  xor r10, r10
  xor r9, r9

  finit

  .ciclo_sen_cos:
    cmp r10d, r11d ;veo si ya llené todo el vector
    je .calcular

    cmp r9d, r15d ;veo si ya llegué a la posición 0 módulo n
    jne .sigo_sen_cos
    xor r9, r9
    .sigo_sen_cos:

    cvtsi2ss xmm0, r9d

    mulss xmm0, xmm2

    divss xmm0, xmm1

    extractps ecx, xmm0, 0

    mov [rsp+ 4*r10], ecx
   	fld dword [rsp+ 4*r10]
    fsin
    fstp dword [rsp+ 4*r10]
	
	add r10, r11
    mov [rsp+ 4*r10], ecx
    fld dword [rsp+ 4*r10]
    fcos
    fstp dword [rsp+ 4*r10]

    sub r10, r11
  inc r9
  inc r10
  jmp .ciclo_sen_cos

  .calcular:

  movdqu xmm15, [alpha]

  movdqu xmm14, [cincuenta]

  movdqu xmm13, [veinticinco]

  pxor xmm12, xmm12 ; para extender


  pxor xmm0, xmm0

  xor r10, r10
  xor r8, r8 ; r8d es la posición en el vector de senos (congruencia módulo n del índice de la fila)

  .ciclo:
    cmp r10d, ebx
    je .fin
    xor r9, r9
    xor rcx, rcx ; ecx es la posición en el vector de cosenos (congruencia módulo n del índice de la columna)
    .ciclo_columnas:
      cmp r9d, r12d
      je .sigo


      movss xmm0, [rsp+ 4*r8] ; traigo los senos a xmm0 (xmm0 = [0|0|0|sen])
      pshufd xmm0, xmm0, 0 ; expando el valor a todo el registro (xmm0 = [sen|sen|sen|sen])
      add rcx, r11
      movaps xmm1, [rsp+ 4*rcx] ; traigo los cosenos a xmm1 (xmm1 = [cos3|cos2|cos1|cos0])
	  sub rcx, r11

      movq xmm2, [r14] ; traigo pixels de memoria a xmm2 (xmm2 = [p3|p2|p1|p0])
      punpcklbw xmm2, xmm12 ; extiendo cada canal a words (xmm2 = [p1|p0])

      mulps xmm0, xmm14
      mulps xmm0, xmm1 ; xmm0 = [sen*cos3*50|sen*cos2*50|sen*cos1*50|sen*cos0*50]

      subps xmm0, xmm13 ; xmm0 = [sen*cos3*50 - 25|sen*cos2*50 - 25|sen*cos1*50 - 25|sen*cos0*50 - 25] = [T3|T2|T1|T0]

      cvtps2dq xmm0, xmm0
      movdqa xmm1, xmm0 ; xmm1 = xmm0

      pshufd xmm0, xmm0, 0 ; xmm0 = [T0|Ŧ0|Ŧ0|Ŧ0]

      psrldq xmm1, 4
      pshufd xmm1, xmm1, 0 ; xmm1 = [T1|Ŧ1|Ŧ1|Ŧ1]

      packssdw xmm0, xmm12 ; xmm0 = [0|0|0|0|T0|Ŧ0|Ŧ0|Ŧ0]
      packssdw xmm1, xmm12 ; xmm1 = [0|0|0|0|T1|Ŧ1|Ŧ1|Ŧ1]

      pslldq xmm1, 8 ; xmm1 = [T1|Ŧ1|Ŧ1|Ŧ1|0|0|0|0]

      por xmm0, xmm1 ; xmm0 = [T1|Ŧ1|Ŧ1|Ŧ1|T0|Ŧ0|Ŧ0|Ŧ0]

      paddw xmm2, xmm0 ; xmm2 = [alpha1+T1|green1+T1|red1+T1|blue1+T1|alpha0+T0|greem0+T0|red0+T0|blue0+T0]

      packuswb xmm2, xmm12

      por xmm2, xmm15 ; canal alpha

      movq [r13], xmm2

      add rcx, 2
      cmp ecx, r15d ; veo si ecx es congruente a 0 módulo n
      jl .avanzo_columna
      xor rcx, rcx

      .avanzo_columna:
      add r14, 8
      add r13, 8
      add r9d, 2
      jmp .ciclo_columnas

  .sigo:
    inc r8
    cmp r8d, r15d ; veo si r8d es congruente a 0 módulo n
    jl .avanzo_fila
    xor r8, r8

    .avanzo_fila:
    inc r10d
    jmp .ciclo

  .fin:
  mov r8, [rbp - 48]
  add rsp, r8
  pop r8
  pop rbx
  pop r15
  pop r14
  pop r13
  pop r12
  pop rbp
  ret
