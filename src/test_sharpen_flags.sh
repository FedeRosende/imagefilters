#!/bin/bash
echo "Ancho,CiclosConsumidos" > unos.vect.Sharpen.c.out
for IMG in ./imgs/*.bmp
do
  for (( i = 0; i < 500; i++ )); do
    WIDTH=$(identify -format '%w' $IMG)
    N_CICLOS=$(./build/tp2 -v Sharpen -i c $IMG)
    echo -n "$WIDTH," >> unos.vect.Sharpen.c.out
    echo "$N_CICLOS" >> unos.vect.Sharpen.c.out
  done
done
