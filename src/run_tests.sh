#!/bin/bash

LONGITUDES=(280, 240, 360, 220, 380, 440, 340, 460, 200, 260, 300, 420, 320, 480, 180, 500, 148, 400,)

for Filtro in Cuadrados Sharpen
do
	I=0
	echo Tamaño,CiclosConsumidos > $Filtro.c.out
	echo Tamaño,CiclosConsumidos > $Filtro.asm.out

	for Imagen in bean.bmp chica.bmp beso.bmp casamiento.bmp coco.bmp diana.bmp dientes.bmp emma.bmp flash.bmp hanks.bmp hawaii.bmp keanu.bmp kunis.bmp lengua.bmp locura.bmp remera.bmp villareal.bmp woody.bmp
	do
		#echo Tamaño,CiclosConsumidos > $Imagen.$Filtro.c01.out
		#echo Tamaño,CiclosConsumidos > $Imagen.$Filtro.asm.out
		for k in {0..599} #cambiar a cantidad de iteraciones deseadas
		do
			VAR=${LONGITUDES[$I]}
			#echo -n $VAR >> $Imagen.$Filtro.c.out
			#echo -n $VAR >> $Imagen.$Filtro.asm.out
			echo -n $VAR >> $Filtro.c.out
			echo -n $VAR >> $Filtro.asm.out

			variableC=$(./build/tp2 -v $Filtro -i c ./imgs/$Imagen)
			variableASM=$(./build/tp2 -v $Filtro -i asm ./imgs/$Imagen)

			#echo $variableC >>  $Imagen.$Filtro.c.out
			echo $variableC >>  $Filtro.c.out

			#echo $variableASM >>  $Imagen.$Filtro.asm.out
			echo $variableASM >>  $Filtro.asm.out
		done
		((I++))
	done
done
